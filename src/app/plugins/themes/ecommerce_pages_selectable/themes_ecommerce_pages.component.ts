import { Component, OnInit } from '@angular/core';
import { Data,Pagination,Succcess } from './themes_ecommerce_pages'
import { ThemesSelectableService } from './themes_ecommerce_pages.service'
import { Router,ActivatedRoute } from '@angular/router';
import { AdviableVar } from '../../../libraries/utils/customvars';
import { Globals } from "../../../system/components/main/globals";
import { Data2 as ThemePageNewData, Data as ThemePageData } from "../../../system/components/themes/themes_page";
import { ThemesService } from "../../../system/components/themes/themes.service";
import { HttpClient, HttpEvent, HttpEventType } from '@angular/common/http';
import { AfterViewInit, ElementRef, EventEmitter, Input, Output, ViewChild } from '@angular/core';
@Component({
  selector: 'app-themes_ecommerce_pages_selectable',
  templateUrl: './themes_ecommerce_pages.component.html',
  styleUrls: ['./themes_ecommerce_pages.component.css']
})

export class ThemePagesSelectableComponent implements AfterViewInit {
  page:number = 1;
  items: Data[];
  theme_page_model: ThemePageNewData;
  query:string = "";
  selected:boolean = false;
  selectedable:boolean = false;
  pages: number = 0;
  @Input() model: number;
 // @Output() modelChange = new EventEmitter<number>()
  constructor(
    private router: Router, 
    private categories:ThemesSelectableService,
    private global:Globals,
    private elementRef: ElementRef,
    private theme_page: ThemesService,

    ) {}
  ngAfterViewInit() {
    //inject dashboard
    this.global.initializeActions();

    this.categories.List(this.page).subscribe((data:Pagination) => {

      this.theme_page.GetThemePage(this.model,"articles").subscribe((data2:ThemePageData) => {

        data.items.forEach(obj => {
            obj.checked = false;
            if(obj.page == data2.page)
            {
              obj.selected = true;
            }
              else
            {
              obj.selected = false;
            }
            
          
         })
        this.items = data.items ;
        this.pages = data.pages;
      })
      
     
    });

  }
  loadmore()
  {
    this.page ++;
    if( this.page >= this.pages)
    {
      this.page = this.pages;
    }
    if(this.query.length >0 )
    {
        this.categories.Search(
          this.page,
          this.query
        ).subscribe((data:Pagination) => {
          data.items.forEach(obj => {
            obj.checked = false;
            obj.selected = false;
         })
        this.items.concat(data.items) ;
        this.pages = data.pages;
      });
    }else
    {
      this.categories.List(this.page).subscribe((data:Pagination) => {
        this.pages = data.pages;
        data.items.forEach(obj => {
            obj.checked = false;
            obj.selected = false;
         })
        this.items.concat(data.items) ;
      });
    }
   
  }

  doSaveThemePage(item:Data)
  {
    //save theme page
    this.theme_page_model = {
      page:item.page,
      module:"articles",
      page_id:this.model
    }
    this.theme_page.UpdateThemePage(this.theme_page_model).subscribe((data:ThemePageData) => {})

  }
  SELECTABLE_SELECT(item:Data){
    if(item.selected == false)
    {
      item.selected = true;
      //save theme page
      this.doSaveThemePage(item);
    }else{
      item.selected = false;
    }
    this.items.forEach(obj => {
        if(obj.page != item.page)
         {
            obj.checked = false;
            obj.selected = false;
         }
      })
    //this.modelChange.emit( item.parent);
  }
  EDIT(item:Data){
    this.router.navigate(['/marketplace/edit/',item.parent ]);
  }

  New(item){
     this.router.navigate(['/marketplace/new' ]);
  }
  find()
  {
 
  
    this.page = 1;

    if(this.query.length > 0 )
    {
        this.categories.Search(
          this.page,
          this.query
        ).subscribe((data:Pagination) => {

         this.items = data.items ;
          this.pages = data.pages;
      });
    }else
    {
      this.categories.List(this.page).subscribe((data:Pagination) => {

        this.items = data.items ;
         this.pages = data.pages;
      });
    }
  }
}
