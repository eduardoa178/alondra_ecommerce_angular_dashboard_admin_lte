


export interface Succcess {

	success: boolean;


}
export interface Data {
	parent:number;
	page:string;
	checked: boolean;
	selected: boolean;

}


export interface Pagination {
	pages: number;
	items: Data[];
	next_page: string;
	prev_page: string;
}