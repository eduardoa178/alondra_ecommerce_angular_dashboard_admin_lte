export interface Succcess {
	success: boolean;
}

export interface ArcheTypes {
	id: string;
	name:string;
}

export interface Data {
	[key: string]: any
}

export interface Pagination {
	pages: number;
	items: Data[];
	next_page: string;
	prev_page: string;
}