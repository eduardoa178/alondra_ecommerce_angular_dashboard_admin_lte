import { Component, OnInit } from '@angular/core';
import { Router,ActivatedRoute } from '@angular/router';
import { Globals } from "../../../system/components/main/globals";
import { BaseSaveComponent } from './basesave.component';
import { Data,ArcheTypes } from './attributes';
import { CustomMethodsService,applyMixins } from '../../../libraries/utils/custommethods.service';
import { AdviableVar } from '../../../libraries/utils/customvars';

export class BaseEditComponent  implements OnInit {
  page:number;
  items: Data[];
  query:string;
  list_url:string;
  selected:boolean;
  pages: number;
  model:Data;
  archetypes: ArcheTypes[];
  constructor(  
    private custom_methods:CustomMethodsService,
    private router: Router,
    private route:ActivatedRoute, 
    private categories:any,
    private global:Globals) { 
   
  }
  ngOnInit() {
      this.global.initializeActions();
      this.initialize();

      this.route.params.subscribe(params =>{
        if (typeof params['id'] !== 'undefined') 
        {       
          
          this.categories.Get(params['id']).subscribe((data:Data) => {
          this.model = data;
          this.global.actions.is_new = false;
        });
        }
        
      });
  }
  initialize()
  {
  }
  FindSlug()
  {
    this.categories.FindSlug(this.model.slug).subscribe((data:AdviableVar) => {

      this.global.actions.exists_slug = data;
    });
  }
  FindTitle()
  {
    this.categories.Find(this.model.title).subscribe((data:AdviableVar) => {

       this.global.actions.exists_title = data;
       this.global.actions.exists_slug = data;

    });
  }
  TitleChanged()
  {
    
    this.model.slug = this.custom_methods.string_to_slug(this.model.title);
    this.model.meta_title = this.model.title;
  }
}

applyMixins(BaseEditComponent, [ BaseSaveComponent]);