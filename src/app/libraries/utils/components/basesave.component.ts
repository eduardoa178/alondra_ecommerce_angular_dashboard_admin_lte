
import { Router,ActivatedRoute } from '@angular/router';
import { Globals } from "../../../system/components/main/globals";
import { Data,ArcheTypes } from './attributes';
import { CustomMethodsService } from '../../../libraries/utils/custommethods.service';
import { AdviableVar } from '../../../libraries/utils/customvars';

export class BaseSaveComponent  {

  list_url:string;

  model:Data;

  constructor(  
    private custom_methods:CustomMethodsService,
    private router: Router,
    private route:ActivatedRoute, 
    private categories:any,
    private global:Globals) { }

  initialize()
  {
  }
 
  save()
  {  
      var that = this;
      var disable_updated_box = function() {
        that.global.actions.updated = false;
      }
      if (this.global.actions.is_new == false)
      {
        this.categories.Update(this.model).subscribe((data:any) => {

          this.global.actions.updated = true;
          setTimeout(disable_updated_box, 3000);
        });
      }else
      {
        this.categories.New(this.model).subscribe((data:Data) => {
          this.model = data;   
          this.global.actions.is_new = false;   
          this.global.actions.updated = true;
          setTimeout(disable_updated_box, 3000);
        });
      }
  }
  
 
  back()
  {
     this.router.navigate([this.list_url ]);
  }
  DELETE()
  {
    this.categories.Delete(this.model.id).subscribe((data:any) => {
      this.router.navigate([this.list_url ]);
    });
    
  }
}
