 import { Component, OnInit } from '@angular/core';
import { Router,ActivatedRoute } from '@angular/router';
import { Globals } from "../../../system/components/main/globals";
import { BaseSaveComponent } from './basesave.component';
import { Data,Pagination } from './attributes';
import { CustomMethodsService,applyMixins } from '../../../libraries/utils/custommethods.service';
import { AfterViewInit, ElementRef, EventEmitter, Input, Output, ViewChild } from '@angular/core';
export class BaseListComponent
{
    page:number;
    items: Data[];
    query:string;
    selected:boolean;
    pages: number;
    edit_url:string;
    new_url:string;
    constructor(
    private router: Router, 
    private global:Globals,
    private categories:any
    ) { }
loadmore()
  {
    this.page=this.page +1;

   if (this.page<this.pages+1)
   {
     if(this.query.length >0 )
      {
        
          this.categories.Search(
            this.page,
            this.query
          ).subscribe((data:Pagination) => {
            data.items.forEach(obj => {
              obj.checked = false;
              obj.selected = false;
           })
           
           this.items.push(...data.items)
           
          this.pages = data.pages;
          this.incrementpage();
        });
      }else
      {
        this.categories.List(this.page).subscribe((data:Pagination) => {
          this.pages = data.pages;
          data.items.forEach(obj => {
              obj.checked = false;
              obj.selected = false;
           })
         this.items.push(...data.items)
          this.incrementpage();
        });
      }
   }
 
   
  }
  incrementpage()
  {
    if( this.page > this.pages)
    {
      this.page = this.pages;
    }
  }

  
  SELECT_ALL()
  {
    if(this.selected == false)
    {
     this.items.forEach(obj => {
        obj.checked = true;
      })
   }else
   {
     this.items.forEach(obj => {
        obj.checked = false;
     })
   }
  }
  DELETED_ALL(){
      var that = this;
      var disable_updated_box = function() {
        that.global.actions.deleted = false;
      }
      this.items.filter(obj => obj.checked == true).forEach(item => {
        this.categories.Delete(item.id).subscribe((data:any) => {
          this.global.actions.deleted = true;
          this.items = this.items.filter(objx => objx !== item);
        });
      })
      setTimeout(disable_updated_box, 3000);
  }
  EDIT(item:Data){

    this.router.navigate([this.edit_url,item.id ]);
  }
  DELETE(item:Data){

       var that = this;
      var disable_updated_box = function() {
        that.global.actions.deleted = false;
      }
  this.global.actions.deleted = true;
          
    this.categories.Delete(item.id).subscribe((data:any) => {
      setTimeout(disable_updated_box, 3000);
      
    });
    this.items = this.items.filter(obj => obj !== item);
  }
  New(item){
     this.router.navigate([this.new_url ]);
  }
  find()
  {
 
  
    this.page = 1;

    if(this.query.length > 0 )
    {
        this.categories.Search(
          this.page,
          this.query
        ).subscribe((data:Pagination) => {

         this.items = data.items ;
          this.pages = data.pages;
      });
    }else
    {
      this.categories.List(this.page).subscribe((data:Pagination) => {

        this.items = data.items ;
         this.pages = data.pages;
      });
    }
  }
}

export class BaseListItemsComponent implements OnInit {
  page:number;
  items: Data[];
  query:string;
  selected:boolean;
  pages: number;
  edit_url:string;
  new_url:string;
  constructor(
    private router: Router, 
    private global:Globals,
    private categories:any,
    ) { 

  }
  ngOnInit() {
    //inject dashboard
    this.global.initializeActions();
    this.page = 1;
    this.query = '';
    this.selected = false;
    this.categories.List(this.page).subscribe((data:Pagination) => {
      data.items.forEach(obj => {
            obj.checked = false;
            obj.selected = false;
         })
       this.items = data.items ;
       this.pages = data.pages;
    });
    
  }

}

export class BaseListItemsAttributableComponent implements OnInit {
  page:number;
  items: Data[];
  query:string;
  selected:boolean;
  pages: number;
  edit_url:string;
  new_url:string;
  constructor(
    private router: Router, 
     private route:ActivatedRoute, 
    private global:Globals,
    private categories:any,
    ) { 

  }
  ngOnInit() {
    //inject dashboard
    this.global.initializeActions();
    this.page = 1;
    this.query = '';
    this.selected = false;
    this.categories.List(this.page).subscribe((data:Pagination) => {
      data.items.forEach(obj => {
            obj.checked = false;
            obj.selected = false;
         })
       this.items = data.items ;
       this.pages = data.pages;
    });
    
  }

}


applyMixins(BaseListItemsComponent, [ BaseListComponent]);

