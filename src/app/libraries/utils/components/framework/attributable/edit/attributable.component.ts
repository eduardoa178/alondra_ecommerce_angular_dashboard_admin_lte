import { Component, OnInit } from '@angular/core';
import { Router,ActivatedRoute } from '@angular/router';
import { Globals } from "../../../../../../system/components/main/globals";
import { BaseEditComponent } from '../../../baseedit.component';
import { CustomMethodsService } from '../../../../../../libraries/utils/custommethods.service';
import { AdviableVar } from '../../../../../../libraries/utils/customvars';
import { BaseService } from '../../../base.service';
import { Data } from '../../../attributes';
@Component({
  selector: 'app-attributable_edit',
  templateUrl: './attributable.component.html',
  styleUrls: ['./attributable.component.css']
})
export class AttributableEditComponent extends BaseEditComponent {
  model:any;
  fields:any;
  MODULE_NAME:string;
  constructor(  
    private custom_methodsx:CustomMethodsService,
    private routerx: Router,
    private routex:ActivatedRoute, 
    private categoriesx:BaseService,
    private globalx:Globals) {
      super(custom_methodsx,routerx,routex,categoriesx,globalx);
    }
    ngOnInit() {
        this.globalx.initializeActions();
        this.initalization();
        this.routex.params.subscribe(params =>{
          if (typeof params['id'] !== 'undefined') 
          { 
            this.categoriesx.Get(params['id']).subscribe((data:Data) => 
            {
              this.setModelValue(data);
              this.globalx.actions.is_new = false;
            });
          }
      });
    }
    initalization()
    {
      this.categoriesx.list_url = this.routex.snapshot.data['list_url'];
      this.categoriesx.search_url = this.routex.snapshot.data['search_url'];
      this.categoriesx.new_url = this.routex.snapshot.data['new_url'];
      this.categoriesx.get_url = this.routex.snapshot.data['get_url'];
      this.categoriesx.find_url = this.routex.snapshot.data['find_url'];
      this.categoriesx.update_url = this.routex.snapshot.data['update_url'];
      this.categoriesx.delete_url = this.routex.snapshot.data['delete_url'];
      this.list_url = this.routex.snapshot.data['redirection_list_url'];
      this.MODULE_NAME = this.routex.snapshot.data['MODULE_NAME'];
      this.model = this.routex.snapshot.data['initalization'];
      this.fields = this.routex.snapshot.data['fields'];
    }

    renderfield(item:Data)
    {
      if(item.renderizable==true)
      {
        return this.model[name].value;
      }
      return "";
    } 

    setModelValue(data:Data)
    {
      var k = Object.keys(data);
      k.forEach(item => {
        this.model[item].value = data;
      })
      return data;
    }

    initializeModel(data:Data)
    {
      data.forEach(item => {
        item.renderizable = true;
        item.type = "text";
        item.adviable = true;
      })
      return data;
    }

    EDIT_STATUS(model:Data)
    {
      model.callback(this);
    }

    FindAdviable(model:Data)
    {
      this.categoriesx.FindAdviable(
          model.id,
          model.value
        ).subscribe((data:AdviableVar) => {
          model.adviable = data;
      });
    }
    
    save()
    {  
      var that = this;
      var disable_updated_box = function() {
        that.globalx.actions.updated = false;
      }
      if (this.globalx.actions.is_new == false)
      {
        this.categoriesx.Update(this.model).subscribe((data:Data) => {
          this.globalx.actions.updated = true;
          setTimeout(disable_updated_box, 3000);
        });
      }else
      {
        this.categoriesx.New(this.model).subscribe((data:Data) => {
          this.setModelValue(data)  
          this.globalx.actions.is_new = false;   
          this.globalx.actions.updated = true;
          setTimeout(disable_updated_box, 3000);
        });
      }
  }
}
