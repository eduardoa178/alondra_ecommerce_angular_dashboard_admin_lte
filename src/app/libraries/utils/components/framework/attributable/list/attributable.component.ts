import { Component, OnInit } from '@angular/core';
import { BaseService } from '../../../base.service'
import { Router,ActivatedRoute } from '@angular/router';
import { Globals } from "../../../../../../system/components/main/globals";
import { BaseListItemsAttributableComponent } from "../../../../../../libraries/utils/components/baselist.component";

@Component({
  selector: 'app-attributable_list',
  templateUrl: './attributble.component.html',
  styleUrls: ['./attributble.component.css']
})
export class ArticlesCategoriesComponent extends BaseListItemsAttributableComponent   {
    page:number;
    items: any[];
    query:string;
    fields:any;
    model:any;
    MODULE_NAME:string;
    selected:boolean;
    pages: number;
    constructor(
      private routerx: Router, 
      private routex:ActivatedRoute, 
      private globalx:Globals,
      private categoriesx:BaseService
    ) { 
      super(routerx,routex,globalx,categoriesx);
   }
   ngOnInit() {
    //inject dashboard
    this.globalx.initializeActions();
    this.initalization();
    this.categoriesx.List(this.page).subscribe((data:any) => {
      data.items.forEach(obj => {
            obj.checked = false;
            obj.selected = false;
         })
      this.items = data.items ;
      this.pages = data.pages;
    });

   }
    initalization()
    {
      this.page = 1;
      this.query = '';
      this.selected = false;
      this.categoriesx.list_url = this.routex.snapshot.data['list_url'];
      this.categoriesx.search_url = this.routex.snapshot.data['search_url'];
      this.categoriesx.new_url = this.routex.snapshot.data['new_url'];
      this.categoriesx.get_url = this.routex.snapshot.data['get_url'];
      this.categoriesx.find_url = this.routex.snapshot.data['find_url'];
      this.categoriesx.update_url = this.routex.snapshot.data['update_url'];
      this.categoriesx.delete_url = this.routex.snapshot.data['delete_url'];
      this.new_url = this.routex.snapshot.data['redirection_new_url'];;
      this.edit_url = this.routex.snapshot.data['redirection_edit_url'];;
      this.MODULE_NAME = this.routex.snapshot.data['MODULE_NAME'];
      this.model = this.routex.snapshot.data['initalization'];

      this.fields = this.routex.snapshot.data['fields'];
    }
    renderfield(item)
    {
      if(item.renderizable==true)
      {
        
        return this.model[name];
      }
      return "";
    } 
}
