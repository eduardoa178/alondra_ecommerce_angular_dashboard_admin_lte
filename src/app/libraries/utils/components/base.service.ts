import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, } from '@angular/common/http';
import { Headers } from '@angular/http';
import {RequestOptions, Request, RequestMethod} from '@angular/http';
import { environment } from '../../../../environments/environment';
import { Globals } from "../../../system/components/main/globals";
import { CookieService } from 'ngx-cookie-service';
import { HttpJsonHeaderOptionsService } from '../httpoptions.service';
import { CustomMethodsService,applyMixins } from '../../../libraries/utils/custommethods.service';

export class BaseServiceActions {

  list_url:string;
  search_url:string;
  new_url:string;
  get_url:string;
  find_url:string;
  find_slug_url:string;
  update_url:string;
  delete_url:string;
  constructor(
    private http2: HttpClient, 
    private cookie2:CookieService,
    private global2:Globals, 
    private header2: HttpJsonHeaderOptionsService) { }



  New(postedData) {
   
    return this.http2
      .post( 
        environment.url + this.new_url,
        postedData,
        this.header2.get_auth(this.global2.token)
      );
  }

  Get(id) {
   
    const postedData = {
      id:id
    }    
    return this.http2
      .post( 
        environment.url + this.get_url,
        postedData,
        this.header2.get_auth(this.global2.token)
      );
  }

  Find(name) {
    
    const postedData = {
      name:name
    }    
    return this.http2
      .post( 
        environment.url + this.find_url,
        postedData,
        this.header2.get_auth(this.global2.token)
      );
  }

  FindSlug(slug) {

    const postedData = {
      slug:slug
    }    
    return this.http2
      .post( 
        environment.url + this.find_slug_url,
        postedData,
        this.header2.get_auth(this.global2.token)
      );
  }

  FindAdviable(key,value) {

    const postedData = {
      key:key,
      value:value
    }    
    return this.http2
      .post( 
        environment.url + this.find_slug_url,
        postedData,
        this.header2.get_auth(this.global2.token)
      );
  }


  Update(postedData) {
   
    return this.http2
      .put( 
        environment.url + this.update_url,
        postedData,
        this.header2.get_auth(this.global2.token)
      );
  }

  Delete(id:number) {

    const postedData = {
      id:id
    }  
    return this.http2
      .delete( 
        environment.url + this.delete_url,
        this.header2.get_auth_params_option(this.global2.token,postedData)
      );
     
  }

}

export class BaseService extends BaseServiceActions {


    constructor(
    private http: HttpClient, 
    private cookie:CookieService,
    private global:Globals, 
    private header: HttpJsonHeaderOptionsService) { 

      super( http,cookie, global,  header )
   }

  List(page) {
     
    const postedData = {page:page}
    return this.http
      .post( 
        environment.url + this.list_url,
        postedData,
        this.header.get_auth(this.global.token)
      );
  }

  Search(page, query) {
    const postedData = {
      page:page,
      query:query
    }
    return this.http
      .post( 
        environment.url + this.search_url,
        postedData,
        this.header.get_auth(this.global.token)
      );
  }
}



