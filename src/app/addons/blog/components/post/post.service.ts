import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, } from '@angular/common/http';
import { Headers } from '@angular/http';
import {RequestOptions, Request, RequestMethod} from '@angular/http';
import { environment } from '../../../../../environments/environment';
import { Globals } from "../../../../system/components/main/globals";
import { CookieService } from 'ngx-cookie-service';
import { HttpJsonHeaderOptionsService } from '../../../../libraries/utils/httpoptions.service';
import { BaseService } from "../../../../libraries/utils/components/base.service";

@Injectable({
  providedIn: 'root'
})
export class PostService  extends BaseService {

   constructor(
    private httpx: HttpClient, 
    private cookiex:CookieService,
    private globalx:Globals, 
    private headerx: HttpJsonHeaderOptionsService) { 
      super(httpx,cookiex, globalx, headerx);
      this.list_url="posts/";
      this.search_url="posts/search/";
      this.new_url="post/";
      this.get_url="post/details/";
      this.find_url="post/find/";
      this.find_slug_url="post/find/slug/";
      this.update_url="post/";
      this.delete_url="post/";
  }
  List(page) {
     
    const postedData = {page:page,post_type:'post'}
    return this.httpx
      .post( 
        environment.url + "posts/",
        postedData,
        this.headerx.get_auth(this.globalx.token)
      );
  }

  Search(page, query) {
    const postedData = {
      page:page,
      query:query,
      post_type:'post'
    }
    return this.httpx
      .post( 
        environment.url + "posts/search/",
        postedData,
        this.headerx.get_auth(this.globalx.token)
      );
  }
}
