import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MarketPlaceBehaviorsStatisticsComponent } from './statistics.component';

describe('MarketPlaceBehaviorsStatisticsComponent', () => {
  let component: MarketPlaceBehaviorsStatisticsComponent;
  let fixture: ComponentFixture<MarketPlaceBehaviorsStatisticsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MarketPlaceBehaviorsStatisticsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MarketPlaceBehaviorsStatisticsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
