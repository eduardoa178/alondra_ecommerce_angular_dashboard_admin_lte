


export interface TopSales {
	id: number;
	sales:number;
	title:string;
	thumbnail:string;
	checked: boolean;
	selected: boolean;
}

export interface PaginationTopSales {
	pages: number;
	items: TopSales[];
	next_page: string;
	prev_page: string;
}

export interface TopSalesYear {
	id: number;
	sales:number;
	mes:string;
	y:string;
	checked: boolean;
	selected: boolean;
}

export interface PaginationTopSalesYear {
	pages: number;
	items: TopSalesYear[];
	next_page: string;
	prev_page: string;
}

