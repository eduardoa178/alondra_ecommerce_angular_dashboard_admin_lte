export interface Data {
	id: number;
	role: string,
	first_name: string,
	last_name: string,
	is_active:boolean,
	is_superuser:boolean,
	username: string,
	email: string,
	nick: string,
	password:string;
	parnetship_requested:boolean;
	parnetship:boolean;
	checked:boolean;
}


export interface ChangePassword {
	id: number;
	old_password:string;
	new_password:string;
	repeat_new_password:string;
}

export interface Role {
	id: string;
	name:string;
}
export interface Pagination {
	pages: number;
	items: Data[];
	next_page: string;
	prev_page: string;
}