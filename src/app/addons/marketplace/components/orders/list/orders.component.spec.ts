import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OrdersMarketplaceComponent } from './orders.component';

describe('OrdersMarketplaceComponent', () => {
  let component: OrdersMarketplaceComponent;
  let fixture: ComponentFixture<OrdersMarketplaceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OrdersMarketplaceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OrdersMarketplaceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
