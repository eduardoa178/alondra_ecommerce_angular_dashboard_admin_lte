

export interface Statuses {
	id: string;
	name:string;
}

export interface MarketplaceItems {
	id: number;
	postx:any;
	sales:number;
	qty:number;
	price:number;
	autorx:any;
	modified:string;
	checked: boolean;
	selected: boolean;
}

export interface MarketplaceSales {
	id: number;
	marketplacex:any;
	autorx:any;
	buyerx:any;
	modified:string;
	checked: boolean;
	selected: boolean;
}

export interface PaginationMarketplaceSales {
	pages: number;
	items: MarketplaceSales[];
	next_page: string;
	prev_page: string;
}

export interface PaginationMarketplaceItems {
	pages: number;
	items: MarketplaceItems[];
	next_page: string;
	prev_page: string;
}




