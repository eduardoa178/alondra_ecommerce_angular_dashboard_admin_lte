 

export interface CdKeysOrders {
	id: number;
	cd_key:string;
}

export interface EmailsDigitalDelivery {
	id: string;
	email:string;
	email_sent:boolean;
}

export interface Addresess {
	id: number;
	city:string;
	country:string;
	zip_code:string;
	first_name:string;
	last_name:string;
	address_line_1:string;
	address_line_2:string;
	address_type:string;
	created:string,
	modified:string,
	checked: boolean;
	selected: boolean;
}

export interface Data {
	id: number;
	thumbnail:string;
	title:string;
	product_type:string;
	get_delivery_methodx:EmailsDigitalDelivery[];
	keys:CdKeysOrders[];
	billing_addresssx:Addresess;
	shipping_addressx:Addresess;	
	price:number;
	qty:number;
	editable:boolean;
	currency:string;
	carrier:string;
	tracking_number:string;
	created:string,
	modified:string,
	checked: boolean;
	selected: boolean;
}

export interface Pagination {
	pages: number;
	items: Data[];
	next_page: string;
	prev_page: string;
}