import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, } from '@angular/common/http';
import { Headers } from '@angular/http';
import {RequestOptions, Request, RequestMethod} from '@angular/http';
import { environment } from '../../../../../environments/environment';
import { Globals } from "../../../../system/components/main/globals";
import { CookieService } from 'ngx-cookie-service';
import { HttpJsonHeaderOptionsService } from '../../../../libraries/utils/httpoptions.service';
import { Data,Pagination } from './discounts';

@Injectable({
  providedIn: 'root'
})
export class DiscountsService {

  
  constructor(
    private http: HttpClient, 
    private cookie:CookieService,
    private global:Globals, 
    private header: HttpJsonHeaderOptionsService) { }

  List(page) {
     
    const postedData = {page:page}
    return this.http
      .post( 
        environment.url + "discounts-codes",
        postedData,
        this.header.get_auth(this.global.token)
      );
  }

  Search(page, query) {
    const postedData = {
      page:page,
      query:query
    }
    return this.http
      .post( 
        environment.url + "discounts-codes/search/",
        postedData,
        this.header.get_auth(this.global.token)
      );
  }

  New(postedData) {
   
    return this.http
      .post( 
        environment.url + "discount-code/",
        postedData,
        this.header.get_auth(this.global.token)
      );
  }

  Get(id) {
   
    const postedData = {
      id:id
    }    
    return this.http
      .post( 
        environment.url + "discount-code/details/",
        postedData,
        this.header.get_auth(this.global.token)
      );
  }

  Find(name) {
    
    const postedData = {
      name:name
    }    
    return this.http
      .post( 
        environment.url + "discount-codes/find/",
        postedData,
        this.header.get_auth(this.global.token)
      );
  }



  Update(postedData) {
   
    return this.http
      .put( 
        environment.url + "discount-code/",
        postedData,
        this.header.get_auth(this.global.token)
      );
  }

  Delete(id:number) {

    const postedData = {
      id:id
    }  
    return this.http
      .delete( 
        environment.url + "discount-code/",
        this.header.get_auth_params_option(this.global.token,postedData)
      );
     
  }

}
