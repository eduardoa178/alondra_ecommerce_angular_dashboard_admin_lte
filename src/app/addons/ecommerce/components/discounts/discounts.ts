

//DISCOUNT CODES
export interface Data {
	id: number;
	code:string;
	amount:number;
	checked: boolean;
	selected: boolean;
}
export interface DataList {
	id: number;
	code:string;
	amount:number;
	used:boolean;
	checked: boolean;
	selected: boolean;
}


export interface Pagination {
	pages: number;
	items: DataList[];
	next_page: string;
	prev_page: string;
}