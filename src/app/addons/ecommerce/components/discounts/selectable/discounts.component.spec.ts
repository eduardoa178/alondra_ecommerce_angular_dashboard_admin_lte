import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DiscountsServiceSelectableComponent } from './discounts.component';

describe('DiscountsServiceSelectableComponent', () => {
  let component: DiscountsServiceSelectableComponent;
  let fixture: ComponentFixture<DiscountsServiceSelectableComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DiscountsServiceSelectableComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DiscountsServiceSelectableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
