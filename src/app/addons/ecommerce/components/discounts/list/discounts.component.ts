import { Component, OnInit } from '@angular/core';
import { DiscountsService } from '../../discounts/discounts.service'
import { DataList,Pagination } from '.././discounts'
import { Router } from '@angular/router';
import { Globals } from "../../../../../system/components/main/globals";
@Component({
  selector: 'app-discounts',
  templateUrl: './discounts.component.html',
  styleUrls: ['./discounts.component.css']
})
export class DiscountsComponent implements OnInit {
  page:number;
  items: DataList[];
  query:string;
  selected:boolean;
  pages: number;
  constructor(
    private router: Router, 
    private categories:DiscountsService,
    private global:Globals
    ) { }
  ngOnInit() {
    //inject dashboard
    this.global.initializeActions();
    this.page = 1;
    this.query = '';
    this.selected = false;
    this.categories.List(this.page).subscribe((data:Pagination) => {
      data.items.forEach(obj => {
            obj.checked = false;
            obj.selected = false;
         })
       this.items = data.items ;
       this.pages = data.pages;
    });
    
  }
  loadmore()
  {
    this.page ++;
    if( this.page >= this.pages)
    {
      this.page = this.pages;
    }
    if(this.query.length >0 )
    {
        this.categories.Search(
          this.page,
          this.query
        ).subscribe((data:Pagination) => {
          data.items.forEach(obj => {
            obj.checked = false;
            obj.selected = false;
         })
        this.items.concat(data.items) ;
        this.pages = data.pages;
      });
    }else
    {
      this.categories.List(this.page).subscribe((data:Pagination) => {
        this.pages = data.pages;
        data.items.forEach(obj => {
            obj.checked = false;
            obj.selected = false;
         })
        this.items.concat(data.items) ;
      });
    }
   
  }
  SELECT_ALL(){
    if(this.selected == false)
    {
      
     this.items.forEach(obj => {
            obj.checked = true;
            
         })
   }else
   {

     this.items.forEach(obj => {
            obj.checked = false;
            
         })
   }
      
  }
  DELETED_ALL(){
    
      var that = this;
      var disable_updated_box = function() {
        that.global.actions.deleted = false;
      }

      this.items.filter(obj => obj.checked == true).forEach(item => {
        this.categories.Delete(item.id).subscribe((data:any) => {
          this.global.actions.deleted = true;
          this.items = this.items.filter(objx => objx !== item);
        });
      })

      setTimeout(disable_updated_box, 3000);
  
      
  }
  EDIT(item:DataList){
    this.router.navigate(['/discounts/edit/',item.id ]);
  }
  DELETE(item:DataList){

       var that = this;
      var disable_updated_box = function() {
        that.global.actions.deleted = false;
      }
  this.global.actions.deleted = true;
          
    this.categories.Delete(item.id).subscribe((data:any) => {
      setTimeout(disable_updated_box, 3000);
      
    });
    this.items = this.items.filter(obj => obj !== item);
  }
  New(item){
     this.router.navigate(['/discounts/new' ]);
  }
  find()
  {
 
  
    this.page = 1;

    if(this.query.length > 0 )
    {
        this.categories.Search(
          this.page,
          this.query
        ).subscribe((data:Pagination) => {

         this.items = data.items ;
          this.pages = data.pages;
      });
    }else
    {
      this.categories.List(this.page).subscribe((data:Pagination) => {

        this.items = data.items ;
         this.pages = data.pages;
      });
    }
  }
}
