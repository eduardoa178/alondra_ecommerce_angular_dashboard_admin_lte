import { Component, OnInit } from '@angular/core';
import { ArticlesCategoryService } from '../../categories/category.service'
import { Data,Pagination } from '.././category'
import { Router } from '@angular/router';
import { Globals } from "../../../../../system/components/main/globals";
import { BaseListItemsComponent } from "../../../../../libraries/utils/components/baselist.component";

@Component({
  selector: 'app-article_categories',
  templateUrl: './categories.component.html',
  styleUrls: ['./categories.component.css']
})
export class ArticlesCategoriesComponent extends BaseListItemsComponent  implements OnInit {
    page:number;
    items: Data[];
    query:string;
    selected:boolean;
    pages: number;
    constructor(
      private  routerx: Router, 
      private globalx:Globals,
      private categoriesx:ArticlesCategoryService
    ) { 
      super(routerx,globalx,categoriesx);
      this.new_url='/articles/categories/new/';
      this.edit_url ='/articles/categories/edit/';
   }
 
}
