import { Component, OnInit } from '@angular/core';
import { UserBehaviorsService } from '../../behaviors/user_behaviors.service'
import { UserMostViewedProducts,UserMostViewedProductsPagination } from '.././behaviors'
import { Router } from '@angular/router';
import { Globals } from "../../../../../system/components/main/globals";
@Component({
  selector: 'app-user_behaviors',
  templateUrl: './behaviors.component.html',
  styleUrls: ['./behaviors.component.css']
})
export class UserBehaviorsComponent implements OnInit {
  page:number;
  items: UserMostViewedProducts[];
  query:string;
  selected:boolean;
  pages: number;
  constructor(
    private router: Router, 
    private categories:UserBehaviorsService,
    private global:Globals
    ) { }
  ngOnInit() {
    //inject dashboard
    this.global.initializeActions();
    this.page = 1;
    this.query = '';
    this.selected = false;
    this.categories.List(this.page).subscribe((data:UserMostViewedProductsPagination) => {
      data.items.forEach(obj => {
            obj.checked = false;
            obj.selected = false;
         })
       this.items = data.items ;
       this.pages = data.pages;
    });
    
  }
  loadmore()
  {
    this.page ++;
    if( this.page >= this.pages)
    {
      this.page = this.pages;
    }
    if(this.query.length >0 )
    {
        this.categories.Search(
          this.page,
          this.query
        ).subscribe((data:UserMostViewedProductsPagination) => {
          data.items.forEach(obj => {
            obj.checked = false;
            obj.selected = false;
         })
        this.items.concat(data.items) ;
        this.pages = data.pages;
      });
    }else
    {
      this.categories.List(this.page).subscribe((data:UserMostViewedProductsPagination) => {
        this.pages = data.pages;
        data.items.forEach(obj => {
            obj.checked = false;
            obj.selected = false;
         })
        this.items.concat(data.items) ;
      });
    }
   
  }
  
}
