import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BehaviorsSearchesComponent } from './behaviors.component';

describe('BehaviorsSearchesComponent', () => {
  let component: BehaviorsSearchesComponent;
  let fixture: ComponentFixture<BehaviorsSearchesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BehaviorsSearchesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BehaviorsSearchesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
