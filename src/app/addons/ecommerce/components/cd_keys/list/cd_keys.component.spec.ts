import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CdKeysComponent } from './cd_keys.component';

describe('CdKeysComponent', () => {
  let component: CdKeysComponent;
  let fixture: ComponentFixture<CdKeysComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CdKeysComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CdKeysComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
