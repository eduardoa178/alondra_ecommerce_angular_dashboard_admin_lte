


export interface Data {
	id: number;
	productx:string;
	product_idx:number;
	product_id:number;
	cd_key:string;
	status:string;
	checked: boolean;
	selected: boolean;
}


export interface Pagination {
	pages: number;
	items: Data[];
	next_page: string;
	prev_page: string;
}