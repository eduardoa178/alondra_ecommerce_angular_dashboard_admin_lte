import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CdkeysSelectableComponent } from './cd_keys.component';

describe('CdkeysSelectableComponent', () => {
  let component: CdkeysSelectableComponent;
  let fixture: ComponentFixture<CdkeysSelectableComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CdkeysSelectableComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CdkeysSelectableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
