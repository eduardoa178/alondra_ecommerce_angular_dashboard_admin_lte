import { Component, OnInit } from '@angular/core';
import { PostService } from '../../post/post.service'
import { Data,Pagination } from '../../post/post'
import { Router } from '@angular/router';
import { Globals } from "../../../../../system/components/main/globals";
import { AfterViewInit, ElementRef, EventEmitter, Input, Output, ViewChild } from '@angular/core';
import { BaseItemSelectableNumberComponent } from "../../../../../libraries/utils/components/baseselectable.component";

@Component({
  selector: 'app-cd_keys_selectable',
  templateUrl: './cd_keys.component.html',
  styleUrls: ['./cd_keys.component.css']
})
export class CdkeysSelectableComponent extends BaseItemSelectableNumberComponent implements AfterViewInit {
  page:number = 1;
  items: Data[];
  @Input() model: number;
  @Output() modelChange = new EventEmitter<number>()
  constructor(
    private routerx: Router, 
    private globalx:Globals,
    private categoriesx:PostService,
    private elementRefx: ElementRef

    ) {

    super(routerx, globalx,categoriesx, elementRefx);
    //this.list_url='/articles/posts/';
    this.edit_url='/articles/posts/edit/';
    this.new_url='/articles/posts/new/';

 }

  loadmore()
  {
    this.page ++;
    if( this.page >= this.pages)
    {
      this.page = this.pages;
    }
    if(this.query.length >0 )
    {
        this.categoriesx.SearchType(
          this.page,
          this.query,
          "digital"
        ).subscribe((data:Pagination) => {
          data.items.forEach(obj => {
            obj.checked = false;
            obj.selected = false;
         })
        this.items.concat(data.items) ;
        this.pages = data.pages;
      });
    }else
    {
      this.categoriesx.ListType(this.page,"digital").subscribe((data:Pagination) => {
        this.pages = data.pages;
        data.items.forEach(obj => {
            obj.checked = false;
            obj.selected = false;
         })
        this.items.concat(data.items) ;
      });
    }
   
  }

  find()
  {
 
  
    this.page = 1;

    if(this.query.length >0 )
    {
        this.categoriesx.SearchType(
          this.page,
          this.query,
          "digital"
        ).subscribe((data:Pagination) => {
          data.items.forEach(obj => {
            obj.checked = false;
            obj.selected = false;
         })
        this.items.concat(data.items) ;
        this.pages = data.pages;
      });
    }else
    {
      this.categoriesx.ListType(this.page,"digital").subscribe((data:Pagination) => {
        this.pages = data.pages;
        data.items.forEach(obj => {
            obj.checked = false;
            obj.selected = false;
         })
        this.items.concat(data.items) ;
      });
    }
  }
}
