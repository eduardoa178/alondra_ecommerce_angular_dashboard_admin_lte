import { Component, OnInit } from '@angular/core';
import { TaxesService } from '../../taxes/taxes.service'
import { Data,Pagination } from '.././taxes'
import { Router } from '@angular/router';
import { Globals } from "../../../../../system/components/main/globals";
import { BaseListItemsComponent } from "../../../../../libraries/utils/components/baselist.component";
@Component({
  selector: 'app-taxes',
  templateUrl: './taxes.component.html',
  styleUrls: ['./taxes.component.css']
})
export class TaxesComponent extends BaseListItemsComponent  implements OnInit {
    page:number;
    items: Data[];
    query:string;
    selected:boolean;
    pages: number;
    constructor(
      private  routerx: Router, 
      private globalx:Globals,
      private categoriesx:TaxesService
    ) { 
      super(routerx,globalx,categoriesx);
      this.new_url='/taxes/new/';
      this.edit_url ='/taxes/edit/';
   }
 
}

