import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TaxesSelectableComponent } from './taxes.component';

describe('TaxesSelectableComponent', () => {
  let component: TaxesSelectableComponent;
  let fixture: ComponentFixture<TaxesSelectableComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TaxesSelectableComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TaxesSelectableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
