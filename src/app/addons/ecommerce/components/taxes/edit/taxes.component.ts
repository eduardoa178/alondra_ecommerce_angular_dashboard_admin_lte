import { Component, OnInit } from '@angular/core';
import { Data } from '.././taxes'
import { TaxesService } from '.././taxes.service'
import { Router,ActivatedRoute } from '@angular/router';
import { CustomMethodsService } from '../../../../../libraries/utils/custommethods.service';
import { AdviableVar } from '../../../../../libraries/utils/customvars';
import { Globals } from "../../../../../system/components/main/globals";
import { BaseEditComponent } from "../../../../../libraries/utils/components/baseedit.component";

@Component({
  selector: 'app-taxes_edit',
  templateUrl: './taxes.component.html',
  styleUrls: ['./taxes.component.css']
})
export class TaxesEditComponent extends BaseEditComponent {
  model:Data;
   constructor(  
    private custom_methodsx:CustomMethodsService,
    private routerx: Router,
    private routex:ActivatedRoute, 
    private categoriesx:TaxesService,
    private globalx:Globals) {
      super(custom_methodsx,routerx,routex,categoriesx,globalx);
      this.list_url = "/taxes/"
    }

    initialize()
    {
      this.model =  {
        city:"New york",
        country:"US",
        percent:0,
        id:null,
        checked:false,
        selected:false,
      };
    }
}
