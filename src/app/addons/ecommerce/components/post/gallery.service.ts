import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, } from '@angular/common/http';
import { Headers } from '@angular/http';
import {RequestOptions, Request, RequestMethod} from '@angular/http';
import { environment } from '../../../../../environments/environment';
import { Globals } from "../../../../system/components/main/globals";
import { CookieService } from 'ngx-cookie-service';
import { HttpJsonHeaderOptionsService } from '../../../../libraries/utils/httpoptions.service';
import { BaseServiceActions } from "../../../../libraries/utils/components/base.service";

@Injectable({
  providedIn: 'root'
})
export class GalleryService  extends BaseServiceActions {
    list_url:string;
    search_url:string;
     constructor(
      private httpx: HttpClient, 
      private cookiex:CookieService,
      private globalx:Globals, 
      private headerx: HttpJsonHeaderOptionsService) { 
      super(httpx,cookiex, globalx, headerx);
      this.list_url="gallery/list/";
      this.search_url="gallery/list/search/";
      this.new_url="gallery/";
      this.get_url="gallery/details/";
      //this.find_url="gallery/find/";
      //this.find_slug_url="gallery/find/slug/";
      this.update_url="gallery/";
      this.delete_url="gallery/";
  }
   List(page,product_id) {
     
    const postedData = {page:page,product_id:product_id}
    return this.httpx
      .post( 
        environment.url + "gallery/list/",
        postedData,
        this.headerx.get_auth(this.globalx.token)
      );
  }

  Search(page, query,product_id) {
    const postedData = {
      page:page,
      query:query,
      product_id:product_id
    }
    return this.httpx
      .post( 
        environment.url + "gallery/list/search/",
        postedData,
        this.headerx.get_auth(this.globalx.token)
      );
  }
}
