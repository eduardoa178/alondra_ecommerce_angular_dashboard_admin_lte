import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ArticlesPostSelectableComponent } from './post.component';

describe('ArticlesPostSelectableComponent', () => {
  let component: ArticlesPostSelectableComponent;
  let fixture: ComponentFixture<ArticlesPostSelectableComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ArticlesPostSelectableComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ArticlesPostSelectableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
