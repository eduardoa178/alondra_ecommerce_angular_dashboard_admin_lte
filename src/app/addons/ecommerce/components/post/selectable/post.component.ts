import { Component, OnInit } from '@angular/core';
import { PostService } from '../../post/post.service'
import { Data,Pagination } from '.././post'
import { Router } from '@angular/router';
import { Globals } from "../../../../../system/components/main/globals";
import { AfterViewInit, ElementRef, EventEmitter, Input, Output, ViewChild } from '@angular/core';
import { BaseListItemSelectableComponent } from "../../../../../libraries/utils/components/baseselectable.component";

@Component({

  selector: 'app-article_posts_selectable',
  templateUrl: './post.component.html',
  styleUrls: ['./post.component.css']
})
export class ArticlesPostSelectableComponent extends BaseListItemSelectableComponent  implements AfterViewInit {

  results: number[] ;
  page:number = 1;
  items: Data[];
  query:string = "";
  selected:boolean = false;
  selectedable:boolean = false;
  pages: number = 0;
  @Input() model: number[];
  @Input() exclude: number;
  @Output() modelChange = new EventEmitter<number[]>()
  constructor(
      private routerx: Router, 
      private globalx:Globals,
      private postx:PostService,
      private elementRefx: ElementRef
    ) {
    super(routerx,globalx,postx,elementRefx );
    this.model = [];
    this.edit_url ="/articles/edit/";
    this.new_url ="/articles/new/";
 }
  ngAfterViewInit() {
    //inject dashboard
    this.globalx.initializeActions();
    this.results = []
    this.postx.List(this.page).subscribe((data:Pagination) => {
      data.items.filter(obj => obj.id != this.exclude).forEach(obj => {
          obj.checked = false;
          obj.selected = false;
          this.model.forEach(objx => {
            if (objx == obj.id)
            {
              obj.selected = true;
              this.results.push(obj.id)
            }
            
          })
         })
      this.items = data.items.filter(obj => obj.id != this.exclude);
      this.pages = data.pages;
    });

  }
  loadmore()
  {
    this.page ++;
    if( this.page >= this.pages)
    {
      this.page = this.pages;
    }
    if(this.query.length >0 )
    {
        this.postx.Search(
          this.page,
          this.query
        ).subscribe((data:Pagination) => {
          data.items.filter(obj => obj.id == this.exclude).forEach(obj => {
            obj.checked = false;
            obj.selected = false;
         })
        this.items.concat(data.items.filter(obj => obj.id != this.exclude)) ;
        this.pages = data.pages;
      });
    }else
    {
      this.postx.List(this.page).subscribe((data:Pagination) => {
        this.pages = data.pages;
        data.items.filter(obj => obj.id == this.exclude).forEach(obj => {
            obj.checked = false;
            obj.selected = false;
         })
        this.items.concat(data.items.filter(obj => obj.id != this.exclude)) ;
      });
    }
   
  }

  SELECTABLE_SELECT(item:Data)
    {
      if(item.selected == false)
      {
         item.selected = true;
      }else{
        item.selected = false;
      }

      this.items.filter(objx => objx.selected === true).forEach(obj => {
        this.results.push(obj.id)
        this.modelChange.emit( this.results);
      })
      
  }

}
