import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, } from '@angular/common/http';
import { Headers } from '@angular/http';
import { RequestOptions, Request, RequestMethod} from '@angular/http';
import { environment } from '../../../../../environments/environment';
import { Globals } from "../../../../system/components/main/globals";
import { CookieService } from 'ngx-cookie-service';
import { HttpJsonHeaderOptionsService } from '../../../../libraries/utils/httpoptions.service';
import { Data,Pagination } from './post';

@Injectable({
  providedIn: 'root'
})
export class MediaFilesService {

  
  constructor(
    private http: HttpClient, 
    private cookie:CookieService,
    private global:Globals, 
    private header: HttpJsonHeaderOptionsService) { }


  List(page,product_id) {
     
    const postedData = {page:page,product_id:product_id}
    return this.http
      .post( 
        environment.url + "archives/",
        postedData,
        this.header.get_auth(this.global.token)
      );
  }

  Search(page, query,product_id) {
    const postedData = {
      page:page,
      query:query,
      product_id:product_id
    }
    return this.http
      .post( 
        environment.url + "archives/search/",
        postedData,
        this.header.get_auth(this.global.token)
      );
  }

  New(postedData) {
   
    return this.http
      .post( 
        environment.url + "archive/",
        postedData,
        this.header.get_auth(this.global.token)
      );
  }

  Get(id) {
   
    const postedData = {
      id:id
    }    
    return this.http
      .post( 
        environment.url + "archive/details/",
        postedData,
        this.header.get_auth(this.global.token)
      );
  }

  Update(postedData) {
   
    return this.http
      .put( 
        environment.url + "archive/",
        postedData,
        this.header.get_auth(this.global.token)
      );
  }
  Upload(postedData) {
   
    return this.http
      .put(
        environment.url + "archive/create/",
        postedData,
        this.header.get_auth_params_option_upload(this.global.token),

      );
  }

  Delete(id:number) {

    const postedData = {
      id:id
    }  
    return this.http
      .delete( 
        environment.url + "archive/",
        this.header.get_auth_params_option(this.global.token,postedData)
      );
     
  }

}
