import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ArticlesPostSelectableFilesComponent } from './post.component';

describe('ArticlesPostSelectableFilesComponent', () => {
  let component: ArticlesPostSelectableFilesComponent;
  let fixture: ComponentFixture<ArticlesPostSelectableFilesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ArticlesPostSelectableFilesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ArticlesPostSelectableFilesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

