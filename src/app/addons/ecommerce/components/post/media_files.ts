export interface Succcess {

	success: boolean;


}

export interface Data {
	id: number;
	title:string;
	product_id:number;
	archive:string;
	selected:boolean;
	checked:boolean;
	selectedable:boolean;
}


export interface Pagination {
	pages: number;
	items: Data[];
	next_page: string;
	prev_page: string;
}


