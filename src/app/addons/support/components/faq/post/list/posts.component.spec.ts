import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SupportFaqComponent } from './post.component';

describe('SupportFaqComponent', () => {
  let component: SupportFaqComponent;
  let fixture: ComponentFixture<SupportFaqComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SupportFaqComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SupportFaqComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
