
import {Data as Category}  from '../categories/category'; 


export interface Data {
	id: number;
	title: string;
	slug: string;
	meta_title: string;
	meta_description: string;
	publish: boolean;
	checked: boolean;
	categoryx:Category[];
	category_id:number;
	category_idx:number;
	related_postsx:number[];
	releated_posts:number[];
	content: string;
	excerpt: string;	
	publish_date: string;
//	featured_start_date: string;
//	featured_end_date: string;
	is_featured: boolean;
	is_on_feed: boolean;
	selected:boolean;
}


export interface Pagination {
	pages: number;
	items: Data[];
	next_page: string;
	prev_page: string;
}


