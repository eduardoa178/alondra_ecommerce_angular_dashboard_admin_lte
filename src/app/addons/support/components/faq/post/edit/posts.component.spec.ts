import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SuportFaqEditComponent } from './post.component';

describe('SuportFaqEditComponent', () => {
  let component: SuportFaqEditComponent;
  let fixture: ComponentFixture<SuportFaqEditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SuportFaqEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SuportFaqEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
