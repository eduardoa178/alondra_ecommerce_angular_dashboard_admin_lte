import { Component, OnInit,AfterViewInit,Input } from '@angular/core';
import { Data} from '.././post'
import { Observable } from 'rxjs';
import { take, switchMap, combineAll,map } from 'rxjs/operators';
import { SupporFaqService } from '../../post/post.service'
import { Router,ActivatedRoute } from '@angular/router';
import { CustomMethodsService } from '../../../../../../libraries/utils/custommethods.service';
import { Globals } from "../../../../../../system/components/main/globals";
import { AdviableVar } from '../../../../../../libraries/utils/customvars';
import { ElementRef,  ViewChild } from '@angular/core';
import {Data as Category}  from '../../categories/category'; 
import {Data as Tags}  from '../../../../../../system/components/tags/tags'; 
import { BaseEditComponent } from "../../../../../../libraries/utils/components/baseedit.component";
import { FaqCategoryService } from '../../categories/category.service'
const SimpleMDE: any = require('simplemde');
declare var jQuery:any;
declare var $:any;
@Component({
  selector: 'app-faq-post_edit',
  templateUrl: './post.component.html',
  styleUrls: ['./post.component.css']
})
export class SuportFaqEditComponent extends BaseEditComponent implements OnInit{
  model:Data;
  mde:any;
  cats:Category[];
  tags:Tags[];
  related:number[];
  exclude:number;
  @ViewChild('simplemde') textarea: ElementRef

  constructor(  
    private custom_methodsx:CustomMethodsService,
    private routerx: Router,
    private routex:ActivatedRoute, 
    private categoriesx:SupporFaqService,
    private categoriesx2:FaqCategoryService,
    
    private globalx:Globals) {
      super(custom_methodsx,routerx,routex,categoriesx,globalx);
      this.list_url = "/support/faq/"
    }


  ngOnInit() {
      
      
      this.globalx.initializeActions();
      this.initialize()
      this.initializeEditor();
      this.categoriesx2.List(1).subscribe((data:any) => {
        this.cats = data.items;
      });
      //asynchronous
       this.routex.params.subscribe(params =>{

        if (typeof params['id'] !== 'undefined') 
        {       
          //asynchronous
            this.exclude = params['id']
            this.categoriesx.Get(params['id']).subscribe((data:Data) => {
              this.model = data;
              this.model.category_id = data.category_idx;
              this.model.releated_posts = data.related_postsx;
              this.related = data.related_postsx;
              this.mde.codemirror.setValue(this.model.content);
              this.globalx.actions.is_new = false;
          });
        }
        
      });
    
        
  }
  initialize()
  {
      this.exclude = 0;
      this.cats = [];
      this.tags = [];
      this.related = [];
      this.model =  {

    
       title:"",
       meta_title:"",
       meta_description:'',
       slug:"",
       content:"",
       excerpt:'',
       publish_date:"",
       //featured_start_date:'',
       //featured_end_date:'',      
       id:null,
  
       publish:true,
       checked:false,
       is_featured:false,
       is_on_feed:false,
       category_idx:0,
       category_id:0,
       categoryx:null,
       related_postsx: null,
       releated_posts: null,
       selected:false,
   
    };
  }
 
  setContent(value:string)
  {
    this.model.content = value;
  }
  setPublishDate(value:string)
  {
    this.model.publish_date = value;
  }
  initializeEditor()
  {
 
    var mde = new SimpleMDE({
      element: this.textarea.nativeElement,
      forceSync: true,
      spellChecker: false,
      status: true
    });
    
   //pass the current instance
    var setValue = this;
    var ChangeEditor = function()
    {

      var value = mde.codemirror.getValue();
    
      setValue.setContent(value) ;  
      
    }
    mde.codemirror.on('change',ChangeEditor);
 
    this.mde = mde;
     function datetimepicker10 (e) {
      setValue.setPublishDate($('#datetimepicker10 input').val())   ;
     }
     $('#datetimepicker10').datetimepicker({ viewMode: 'years',format: 'YYYY-MM-DD H:mm:ss'}).on("dp.change",datetimepicker10 );
     console.log($('#datetimepicker10').datetimepicker)
  }
  


  save()
  {  
    var that = this;
    

    this.model.releated_posts = this.related;

    var disable_updated_box = function() {
      that.globalx.actions.updated = false;
    }
    if (this.globalx.actions.is_new == false)
    {
       
      this.categoriesx.Update(this.model).subscribe((data:any) => {
        this.model.category_id = data.category_idx;
        this.globalx.actions.updated = true;
        setTimeout(disable_updated_box, 3000);
      });
    }else
    {
      this.categoriesx.New(this.model).subscribe((data:Data) => {
        this.model = data;   
        this.model.category_id = data.category_idx;
        this.globalx.actions.is_new = false;   
        this.globalx.actions.updated = true;
        setTimeout(disable_updated_box, 3000);
      });
    }
  }


}
