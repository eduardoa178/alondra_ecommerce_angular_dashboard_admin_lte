import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SupportFaqCategoriesComponent } from './categories.component';

describe('SupportFaqCategoriesComponent', () => {
  let component: SupportFaqCategoriesComponent;
  let fixture: ComponentFixture<SupportFaqCategoriesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SupportFaqCategoriesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SupportFaqCategoriesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
