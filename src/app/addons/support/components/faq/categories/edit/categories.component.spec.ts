import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SupportFaqCategoriesEditComponent } from './categories.component';

describe('SupportFaqCategoriesEditComponent', () => {
  let component: SupportFaqCategoriesEditComponent;
  let fixture: ComponentFixture<SupportFaqCategoriesEditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SupportFaqCategoriesEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SupportFaqCategoriesEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
