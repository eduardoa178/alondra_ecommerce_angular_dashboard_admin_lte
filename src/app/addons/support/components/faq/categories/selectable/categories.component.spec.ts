import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SupportFaqCategoriesSelectableComponent } from './categories.component';

describe('SupportFaqCategoriesSelectableComponent', () => {
  let component: SupportFaqCategoriesSelectableComponent;
  let fixture: ComponentFixture<SupportFaqCategoriesSelectableComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SupportFaqCategoriesSelectableComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SupportFaqCategoriesSelectableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
