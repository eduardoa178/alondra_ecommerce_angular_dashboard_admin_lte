import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

//addons/blog
import { PostComponent } from '../addons/blog/components/post/list/post.component';
import { PostEditComponent } from '../addons/blog/components/post/edit/post.component';

import { PageComponent } from '../addons/blog/components/page/list/page.component';
import { PageEditComponent } from '../addons/blog/components/page/edit/page.component';

import { CommentsComponent } from '../addons/blog/components/comments/comments.component';

import { CategoriesComponent } from '../addons/blog/components/categories/list/categories.component';
import { CategoriesEditComponent } from '../addons/blog/components/categories/edit/categories.component';

//addons/ecommerce
import { StatisticsComponent } from '../addons/ecommerce/components/statistics/dashboard/statistics.component';
import { BehaviorsComponent } from '../addons/ecommerce/components/behaviors/list/behaviors.component';
import { UserBehaviorsComponent } from '../addons/ecommerce/components/behaviors/user/behaviors.component';
import { BehaviorsSearchesComponent } from '../addons/ecommerce/components/behaviors/searches/behaviors.component';

import { ArticlesPostComponent } from '../addons/ecommerce/components/post/list/post.component';
import { ArticlesPostEditComponent } from '../addons/ecommerce/components/post/edit/post.component';

import { ArticlesCategoriesComponent } from '../addons/ecommerce/components/categories/list/categories.component';
import { ArticlesCategoriesEditComponent } from '../addons/ecommerce/components/categories/edit/categories.component';

import { AttributesComponent } from '../addons/ecommerce/components/attributes/list/attributes.component';
import { AttributesEditComponent } from '../addons/ecommerce/components/attributes/edit/attributes.component';

import { TaxesComponent } from '../addons/ecommerce/components/taxes/list/taxes.component';
import { TaxesEditComponent } from '../addons/ecommerce/components/taxes/edit/taxes.component';

import { DiscountsComponent } from '../addons/ecommerce/components/discounts/list/discounts.component';
import { DiscountsEditComponent } from '../addons/ecommerce/components/discounts/edit/discounts.component';

import { CdKeysComponent } from '../addons/ecommerce/components/cd_keys/list/cd_keys.component';
import { CdkeysEditComponent } from '../addons/ecommerce/components/cd_keys/edit/cd_keys.component';

import { OrdersComponent } from '../addons/ecommerce/components/orders/list/orders.component';
import { OrdersEditComponent } from '../addons/ecommerce/components/orders/edit/orders.component';

//addons/support
import { SupportFaqComponent } from '../addons/support/components/faq/post/list/post.component';
import { SuportFaqEditComponent } from '../addons/support/components/faq/post/edit/post.component';

import { SupportFaqCategoriesComponent } from '../addons/support/components/faq/categories/list/categories.component';
import { SupportFaqCategoriesEditComponent } from '../addons/support/components/faq/categories/edit/categories.component';

import { AppComponent } from '../system/components/main/app.component';

import { SigninupComponent } from '../system/components/auth/signinup/signinup.component';
import { LoginComponent } from '../system/components/auth/login/login.component';

import { DashboardComponent } from '../system/components/dashboard/dashboard.component';

import { MediaComponent } from '../system/components/media/list/media.component';
import { MediaEditComponent } from '../system/components/media/edit/media.component';

import { TagsComponent } from '../system/components/tags/list/tags.component';
import { TagsEditComponent } from '../system/components/tags/edit/tags.component';

import { UsersComponent } from '../system/components/users/list/users.component';
import { UsersEditComponent } from '../system/components/users/edit/users.component';

import { NavigationsComponent } from '../system/components/navigations/list/navigations.component';
import { NavigationsEditComponent } from '../system/components/navigations/edit/navigations.component';

import { NavigationsItemsComponent } from '../system/components/navigations_items/list/navigations_items.component';
import { NavigationsItemsEditComponent } from '../system/components/navigations_items/edit/navigations_items.component';

import { SettingsComponent } from '../system/components/settings/settings.component';

import { PermissionComponent } from '../system/components/permissions/permissions/list/permissions.component';
import { PermissionsEditComponent } from '../system/components/permissions/permissions/edit/permissions.component';

import { GroupsComponent } from '../system/components/permissions/groups/list/groups.component';
import { GroupsEditComponent } from '../system/components/permissions/groups/edit/groups.component';

import { UserPermissionComponent } from '../system/components/permissions/user_permissions/list/user_permissions.component';
import { UserPermissionEditComponent } from '../system/components/permissions/user_permissions/edit/user_permissions.component';

import { TranslationsComponent } from '../system/components/translations/list/translations.component';
import { TranslationsEditComponent } from '../system/components/translations/edit/translations.component';

import { ThemesComponent } from '../system/components/themes/themes.component';
///marketplace

import { OrdersMarketplaceComponent } from '../addons/marketplace/components/orders/list/orders.component';
import { UsersMarketplaceComponent } from '../addons/marketplace/components/users/list/users.component';
import { MarketPlaceBehaviorsStatisticsComponent } from '../addons/marketplace/components/statistics/dashboard/statistics.component';


const routes: Routes = [

  
  { path: 'dashboard', component: DashboardComponent },

  { path: 'newaccount', component: SigninupComponent },
  { path: 'login', component: LoginComponent },
  
  //addons/blog  
  { path: 'posts', component: PostComponent },
  { path: 'posts/edit/:id', component: PostEditComponent },
  { path: 'posts/new', component: PostEditComponent },

  { path: 'pages', component: PageComponent },
  { path: 'pages/edit/:id', component: PageEditComponent },
  { path: 'pages/new', component: PageEditComponent },

  { path: 'categories', component: CategoriesComponent},
  { path: 'categories/new', component: CategoriesEditComponent},
  { path: 'categories/edit/:id', component: CategoriesEditComponent },

  //addons/ecommerce

  { path: 'articles/statistics', component: StatisticsComponent },
  { path: 'articles/statistics/most-viewed', component: BehaviorsComponent },
  { path: 'articles/statistics/most-viewed/user', component: UserBehaviorsComponent },
  { path: 'articles/statistics/searches', component: BehaviorsSearchesComponent },

  { path: 'articles/posts', component: ArticlesPostComponent },
  { path: 'articles/posts/edit/:id', component: ArticlesPostEditComponent },
  { path: 'articles/posts/new', component: ArticlesPostEditComponent },

  { path: 'articles/categories', component: ArticlesCategoriesComponent },
  { path: 'articles/categories/new', component: ArticlesCategoriesEditComponent },
  { path: 'articles/categories/edit/:id', component: ArticlesCategoriesEditComponent },

  { path: 'attributes', component: AttributesComponent },
  { path: 'attributes/edit/:id', component: AttributesEditComponent },
  { path: 'attributes/new', component: AttributesEditComponent },

  { path: 'taxes', component: TaxesComponent },
  { path: 'taxes/edit/:id', component: TaxesEditComponent },
  { path: 'taxes/new', component: TaxesEditComponent },

  { path: 'discounts', component: DiscountsComponent },
  { path: 'discounts/edit/:id', component: DiscountsEditComponent },
  { path: 'discounts/new', component: DiscountsEditComponent },

  { path: 'cd_keys', component: CdKeysComponent },
  { path: 'cd_keys/edit/:id', component: CdkeysEditComponent },
  { path: 'cd_keys/new', component: CdkeysEditComponent },

  { path: 'orders', component: OrdersComponent },
  { path: 'orders/view/:id', component: OrdersEditComponent },


  //adddons/support
  
  { path: 'support/faq', component: SupportFaqComponent },
  { path: 'support/faq/edit/:id', component: SuportFaqEditComponent },
  { path: 'support/faq/new', component: SuportFaqEditComponent },

  { path: 'support/faq/categories', component: SupportFaqCategoriesComponent },
  { path: 'support/faq/categories/edit/:id', component: SupportFaqCategoriesEditComponent },
  { path: 'support/faq/categories/new', component: SupportFaqCategoriesEditComponent },
  //system
  


  
  
    
  

  { path: 'permisions/groups', component: GroupsComponent },
  { path: 'permisions/groups/edit/:id', component: GroupsEditComponent },
  { path: 'permisions/groups/new', component: GroupsEditComponent },

  { path: 'permisions', component: PermissionComponent },
  { path: 'permisions/edit/:id', component: PermissionsEditComponent },
  { path: 'permisions/new', component: PermissionsEditComponent },

  //{ path: 'user/permisions-groups', component: UserPermissionComponent },
  { path: 'user/permisions-groups/:parent', component: UserPermissionEditComponent },
 
  { path: 'translations', component: TranslationsComponent },
  { path: 'translations/edit/:id', component: TranslationsEditComponent },
  { path: 'translations/new', component: TranslationsEditComponent },

  { path: 'media', component: MediaComponent },
  { path: 'media/edit/:id', component: MediaEditComponent },
  { path: 'tags', component: TagsComponent },
  { path: 'tags/edit/:id', component: TagsEditComponent },
  { path: 'tags/new', component: TagsEditComponent },
  { path: 'users', component: UsersComponent },
  { path: 'users/edit/:id', component: UsersEditComponent },
  { path: 'users/new', component: UsersEditComponent },
  { path: 'profile', component: UsersEditComponent },


  { path: 'navigations', component: NavigationsComponent },
  { path: 'navigations/new', component: NavigationsEditComponent },
  { path: 'navigations/edit/:id', component: NavigationsEditComponent },
  { path: 'navigations/items/edit/:id/:parent', component: NavigationsItemsEditComponent },
  { path: 'navigations/items/new/:parent', component: NavigationsItemsEditComponent },
  { path: 'comments', component: CommentsComponent },
  { path: 'settings', component: SettingsComponent },
  { path: 'themes', component: ThemesComponent },

//

  { path: 'marketplace/user-orders/:id/list', component: OrdersMarketplaceComponent },
  { path: 'marketplace/users/list', component: UsersMarketplaceComponent },
  { path: 'marketplace/statistics', component: MarketPlaceBehaviorsStatisticsComponent },


  { path:  '**', component: DashboardComponent },
  { path: '',   redirectTo: '/login', pathMatch: 'full'},

];

@NgModule({
  imports: [
    ///RouterModule.forRoot(routes) 
    RouterModule.forRoot(
      routes,
      { enableTracing: false } // <-- debugging purposes only
    )
  ],
  exports: [ RouterModule ],
  declarations: []
})
export class AppRoutingModule { }
