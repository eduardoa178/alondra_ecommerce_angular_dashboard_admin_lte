export interface Data {
	id: number;
	name: string;
	publish: boolean;
	checked: boolean;
	selected: boolean;
	navigation:any;
}


export interface Pagination {
	pages: number;
	items: Data[];
	next_page: string;
	prev_page: string;
}


