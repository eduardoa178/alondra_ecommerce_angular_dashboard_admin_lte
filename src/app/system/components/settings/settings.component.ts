import { Component, OnInit } from '@angular/core';
import { Data } from './settings'
import { SettingsService } from './settings.service'
import { Router,ActivatedRoute } from '@angular/router';
import { CustomMethodsService } from '../../../libraries/utils/custommethods.service';
import { AdviableVar } from '../../../libraries/utils/customvars';
import { Globals } from "../main/globals";

@Component({
  selector: 'app-settings',
  templateUrl: './settings.component.html',
  styleUrls: ['./settings.component.css']
})
export class SettingsComponent implements OnInit {
  model:Data;
  parent:number;
  constructor(  
    private custom_methods:CustomMethodsService,
    private router: Router,
    private route:ActivatedRoute, 
    private settigns:SettingsService,
    private global:Globals) { }


  ngOnInit() {
      this.global.initializeActions();
      this.initialize();
     
         
          this.settigns.Get().subscribe((data:Data) => {
          this.model = data;
          this.global.actions.is_new = false;
      
        
      });
    
  }
  initialize()
  {
    this.model =  {
      background_image:"",
      website_name:"",
      perpage_home:0,
      perpage_category:0,
      description:"",
      dollar_price:0,
      id:null,
      checked:false,
      selected:false
    };
  }
  save()
  {  
      var that = this;
      var disable_updated_box = function() {
        that.global.actions.updated = false;
      }
      if (this.global.actions.is_new == false)
      {
        this.settigns.Update(this.model).subscribe((data:any) => {

          this.global.actions.updated = true;
          setTimeout(disable_updated_box, 3000);
        });
      }
    
     
    
  }


}
