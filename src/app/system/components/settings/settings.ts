
export interface Data {
	id: number;
	website_name: string;
	perpage_home: number;
	perpage_category: number;
	description: string;
	background_image:string;
	dollar_price:number;
	checked: boolean;
	selected:boolean;
}

export interface Pagination {
	pages: number;
	items: Data[];
	next_page: string;
	prev_page: string;
}