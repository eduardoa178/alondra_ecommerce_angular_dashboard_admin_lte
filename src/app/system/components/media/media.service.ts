import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, } from '@angular/common/http';
import { Headers } from '@angular/http';
import {RequestOptions, Request, RequestMethod} from '@angular/http';
import { environment } from '../../../../environments/environment';
import { Globals } from "../main/globals";
import { CookieService } from 'ngx-cookie-service';
import { HttpJsonHeaderOptionsService } from '../../../libraries/utils/httpoptions.service';


@Injectable({
  providedIn: 'root'
})
export class MediaService {

  
    
  
  constructor(
    private http: HttpClient, 
    private cookie:CookieService,
    private global:Globals, 
    private header: HttpJsonHeaderOptionsService) { }

   List(query) {
     
    const postedData = {query:query}
    return this.http
      .post( 
        environment.url + "images/",
        postedData,
        this.header.get_auth(this.global.token)
      );
  }
  ListFolder(query) {
     
    const postedData = {query:query}
    return this.http
      .post( 
        environment.url + "folders/",
        postedData,
        this.header.get_auth(this.global.token)
      );
  }



  DeleteDir(folder) {
   const postedData =  {'folder':folder}      
    return this.http
      .delete( 
        environment.url + "folder/",
       this.header.get_auth_params_option(this.global.token,postedData)
        
      );
  }
  DeleteFile(query,file) {
   const postedData = {'query':query,'file':file} 
    return this.http
      .delete( 
        environment.url + "file/",
        this.header.get_auth_params_option(this.global.token,postedData)
      );
  }
  UpdateFile(postedData) {
 

    return this.http
      .put( 
        environment.url + "image/create/",
        postedData,
        this.header.get_auth_params_option_upload(this.global.token),
      );
  }
  UpdateDir(query,folder) {
   const postedData = {'query':query,'folder':folder} 
    return this.http
      .put( 
        environment.url + "folder/",
        postedData,
        this.header.get_auth(this.global.token)
      );
  }
  SaveDir(query,folder,newname) {
   const postedData = {
      'query':query,
      'folder':folder,
      'newname':newname



    } 
    return this.http
      .post( 
        environment.url + "folder/",
        postedData,
        this.header.get_auth(this.global.token)
      );
  }

  NewFile(postedData) {
 
    return this.http
      .post( 
        environment.url + "file/",
        postedData,
        this.header.get_auth(this.global.token)
      );
  }
   


}
