
export interface Data {
	id: number;
	pk:number;
	oldname:string;
	name: string;
	url_path:string;
	checked:boolean;
	selected:boolean;
	duplicate:boolean;
	editable:boolean;
	folder:boolean;
}


export interface Pagination {
	pages: number;
	items: Data[];
	next_page: string;
	prev_page: string;
}


