import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Location } from '@angular/common';
import { LoginService } from '../login.service';
import { Globals } from "../../main/globals";
import { Reset } from '../users';


@Component({
  selector: 'app-reset',
  templateUrl: './reset.component.html',
  styleUrls: ['./reset.component.css']
})
export class ResetComponent implements OnInit {

  model:Reset;
  constructor( 
    private router: Router,
    private categories: LoginService,
    private global:Globals
  ){}

  ngOnInit() 
  {}
  Reset()
  {

    var that = this;
    var disable_updated_box = function() {
      that.global.actions.updated = false;
    }
    if (this.global.actions.is_new == false)
    {
      this.categories.Reset(this.model).subscribe((data:any) => {
        this.global.actions.updated = true;
        setTimeout(disable_updated_box, 3000);
      });
    }
  }
  Loginx()
  {
    this.router.navigate(['/login']);
  }
}
