import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Location } from '@angular/common';
import { Globals } from "../../main/globals";
import { LoginService } from '../login.service';

interface Data {
  token: string;
}
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {


  username: string;
  password: string;
  remember_me: boolean;

  constructor( 
    private router: Router,
    private loginService: LoginService,
    private globals:Globals
  ) 
  {}

  ngOnInit() 
  {
 
    if(this.globals.get_token()==true)
    {
      this.router.navigate(['/articles/posts']);
    }
  }
  Loginx()
  {


    this
      .loginService
      .getUser(this.username,this.password)
      .subscribe((data:Data) => {

        if (typeof data.token !== 'undefined') 
        {
          this.globals.set_token(data.token, this.remember_me);
          this.router.navigate(['/articles/posts']);
        }

      });
  	
  }
  Registerx()
  {
    this.router.navigate(['/newaccount']);
  }
}
