import { Component, OnInit } from '@angular/core';
import { Data,Pagination,Theme,Succcess } from './themes'
import { ThemesService } from './../themes/themes.service'
import { Router,ActivatedRoute } from '@angular/router';
import { AdviableVar } from '../../../libraries/utils/customvars';
import { Globals } from "../main/globals";
import { ElementRef,  ViewChild } from '@angular/core';
import { HttpClient, HttpEvent, HttpEventType } from '@angular/common/http';
declare var jQuery:any;
declare var $:any;
@Component({
  selector: 'app-themes',
  templateUrl: './themes.component.html',
  styleUrls: ['./themes.component.css']
})
export class ThemesComponent implements OnInit {
  model:Pagination;
  settings:Data;
  progresss:number;
  page:number;
  pages: number;
  query:string;
  uploadedPercentage :number;
  constructor(  
    private router: Router,
    private route:ActivatedRoute, 
    private categories:ThemesService,
    private global:Globals) { }


  ngOnInit() {
    this.global.initializeActions();
    this.initialize();
    this.initializeEditor();
    this.page = 1;
    this.pages = 0;
    this.query = '';
    this.loadnewThemes();
  }


  loadnewThemes()
  {
    this.categories.Get().subscribe((data:Data)=> {

      this.settings.theme_name = data.theme_name
        this.categories.List(1).subscribe((data:Pagination) => {
          data.items.forEach(obj => {
              if(this.settings.theme_name == obj.theme)
              {
                obj.checked = true;
                obj.selected = true;
              }else{
                obj.checked = false;
                obj.selected = false;
              }
              
           })
          this.model.items = data.items ;
          this.pages = data.pages;
        });

      })
      
  }


  Upload(files:FileList[]) {
      

      var fd = new FormData();
      jQuery.each(files,function(i,val){
        fd.append("file",val)
      })
     this.categories.Upload(fd).subscribe( (event: Succcess) => {

    
      if(  event.success == true)
      {
        this.loadnewThemes();
      }
    });

      
    };
  initializeEditor()
  { 
    /*
      pending create ajax with angular progressbar ajax put
    */       

    this.progresss = 0;
    var that = this;
   
    var open_uploader = function (e)
    {  
      e.preventDefault();
      e.stopPropagation();
      jQuery('#id_files').focus().trigger('click');
    }

    jQuery("#holder").on('click',open_uploader);
    var clicks = function(e)
    {
      e.stopPropagation();
    }
    jQuery('#id_files').on('click',clicks);
    
    jQuery('#id_files').bind('change', function (e) {
      e.stopPropagation();
      var files:FileList[] = e.target.files;
      that.Upload(files);
    });

    jQuery("#holder").on('drop', function(e) {
      e.preventDefault();
      e.stopPropagation();
      
      if (e.dataTransfer){
          if (e.dataTransfer.files.length > 0) {
             var files:FileList[] = e.dataTransfer.files;
              that.Upload(files);
          }
      }
      return false;
    });
    jQuery("#holder").on('dragover', function(e) {
        e.preventDefault();
        e.stopPropagation();
    });
    jQuery("#holder").on('dragenter', function(e) {
        e.preventDefault();
        e.stopPropagation();
    });




  }


initialize()
  {
    this.settings =  {
      id:0,
      theme_name:"",
    };
    this.model = {
      pages: 0,
      items: [],
      next_page:"",
      prev_page:""
    }
  }
  itemSelected(item:Theme)
  {
    return item.selected == true;
  }

 loadmore()
  {
    this.page ++;
    if( this.page >= this.pages)
    {
      this.page = this.pages;
    }
    if(this.query.length >0 )
    {
        /*this.categories.Search(
          this.page,
          this.query
        ).subscribe((data:Pagination) => {
          data.items.forEach(obj => {
            obj.checked = false;
            obj.selected = false;
         })
        this.model.items.concat(data.items) ;
        this.pages = data.pages;
      });*/
    }else
    {
      this.categories.List(this.page).subscribe((data:Pagination) => {
        this.pages = data.pages;
        this.model.items.concat(data.items) ;
      });
    }
   
  }
  find()
  {
 
  
    this.page = 1;

    if(this.query.length > 0 )
    {
        /*this.categories.Search(
          this.page,
          this.query
        ).subscribe((data:Pagination) => {

         this.model.items = data.items ;
         this.pages = data.pages;
         
      });*/
    }else
    {
      this.categories.List(this.page).subscribe((data:Pagination) => {

         this.model.items = data.items ;
         this.pages = data.pages;
      });
    }
  }


  back()
  {
     this.router.navigate(['/users' ]);
  }
  DELETE(item:Data)
  {
   
    this.categories.Delete(item.theme_name).subscribe((data:any) => {
      this.router.navigate(['/users' ]);

    });
    
  }
  SELECT(item:Theme)
  {
    
      item.selected = true; 
      this.settings.theme_name = item.theme;
      this.categories.Update(this.settings).subscribe((data:any) => {})
      this.model.items.forEach(obj => {
        if(obj.theme != item.theme)
         {
            obj.checked = false;
            obj.selected = false;
         }
       
      })

   
  }
}
