
export interface Theme {

	theme:string;
	checked: boolean;
	selected: boolean;

}


export interface Succcess {

	success: boolean;


}
export interface Data {
	id:number;

	theme_name:string;

}


export interface Pagination {
	pages: number;
	items: Theme[];
	next_page: string;
	prev_page: string;
}