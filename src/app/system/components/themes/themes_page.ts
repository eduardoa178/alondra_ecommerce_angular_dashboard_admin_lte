

export interface Succcess {
	success: boolean;
}
export interface Data {
	id:number;
	page: string;
	module: string;
	page_id: number;
}

export interface Data2 {
	
	page: string;
	module: string;
	page_id: number;
}

export interface Pagination {
	pages: number;
	items: Data[];
	next_page: string;
	prev_page: string;
}