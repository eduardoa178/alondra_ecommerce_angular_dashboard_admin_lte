import { Component, OnInit } from '@angular/core';
import { Router,ActivatedRoute } from '@angular/router';
import { CustomMethodsService } from '../../../../../libraries/utils/custommethods.service';
import { AdviableVar } from '../../../../../libraries/utils/customvars';
import { Globals } from "../../../main/globals";
import { Data as Groups, Pagination as GroupsPagination } from '../../groups/groups';
import { GroupService } from '../../groups/groups.service'
import { Data } from '.././user_permissions'
import { UserPermissionService } from '.././user_permissions.service';

@Component({
  selector: 'app-user_permissions_edit',
  templateUrl: './user_permissions.component.html',
  styleUrls: ['./user_permissions.component.css']
})
export class UserPermissionEditComponent implements OnInit {
  model:Data;
  groups_lists:Groups[];
  page:number;
  pages: number;
  query:string;
  parent: number;
  constructor(  
    private custom_methods:CustomMethodsService,
    private router: Router,
    private route:ActivatedRoute, 
    private groups:GroupService,
    private categories:UserPermissionService,
    private global:Globals) { }


  ngOnInit() {
      this.global.initializeActions();
      this.initialize();
      this.page = 1;
      this.pages = 0;
      this.query = '';
      this.route.params.subscribe(params =>{

      this.categories.List(1).subscribe((data:GroupsPagination) => {
          this.groups_lists = data.items;
          this.pages = data.pages;
          this.groups_lists.forEach(obj => {
            obj.selected = false;
          });
      
          if (typeof params['parent'] !== 'undefined') 
          {       
            this.model.userid = params['parent'];
            this.groups.Get(params['parent']).subscribe((data:Data) => {
              if(data.id > 0 )
              {
                this.model = data;
                this.model.userid = params['parent'];
                this.model.groups_lists.forEach(obj => {
                  obj.selected = true;
                  this.groups_lists.forEach(objx => {
                    if(objx.id == obj.id)
                    {
                      objx.selected = true;
                    }
                  });
                });
              }
              this.global.actions.is_new = false;
            });
          }
      });
    });
    
  }

  initialize()
  {
    this.groups_lists = [];
    this.model =  {
      userid:null,
      groups_lists:[],
      id:null,
      checked:false,
      selected:false,
    };
  }
  save()
  {  
    var that = this;
    var disable_updated_box = function() {
      that.global.actions.updated = false;
    }
    if (this.global.actions.is_new == false)
    {
      this.groups.Update(this.model).subscribe((data:any) => {
        this.global.actions.updated = true;
        setTimeout(disable_updated_box, 3000);
      });
    }else
    {
      this.groups.New(this.model).subscribe((data:Data) => {
        this.model = data;   
        this.global.actions.is_new = false;   
        this.global.actions.updated = true;
        setTimeout(disable_updated_box, 3000);
      });
    }
  }
  itemSelected(item:Groups)
  {
    return item.selected == true;
  }

 loadmore()
  {
    this.page ++;
    if( this.page >= this.pages)
    {
      this.page = this.pages;
    }
    if(this.query.length >0 )
    {
        this.categories.Search(
          this.page,
          this.query
        ).subscribe((data:GroupsPagination) => {
          data.items.forEach(obj => {
            obj.checked = false;
            obj.selected = false;
         })
        this.groups_lists.concat(data.items) ;
        this.pages = data.pages;
      });
    }else
    {
      this.categories.List(this.page).subscribe((data:GroupsPagination) => {
        this.pages = data.pages;
        data.items.forEach(obj => {
            obj.checked = false;
            obj.selected = false;
         })
        this.groups_lists.concat(data.items) ;
      });
    }
   
  }
  find()
  {
 
  
    this.page = 1;

    if(this.query.length > 0 )
    {
        this.categories.Search(
          this.page,
          this.query
        ).subscribe((data:GroupsPagination) => {

         this.groups_lists = data.items ;
          this.pages = data.pages;
           this.groups_lists.forEach(obj => {
            obj.selected = false;
          })
          this.model.groups_lists.forEach(obj => {
            obj.selected = true;
            this.groups_lists.forEach(objx => {
              if(objx.id == obj.id)
              {
                objx.selected = true;
              }
            })
          })
      });
    }else
    {
      this.categories.List(this.page).subscribe((data:GroupsPagination) => {

        this.groups_lists = data.items ;
         this.pages = data.pages;
          this.groups_lists.forEach(obj => {
            obj.selected = false;
          })
          this.model.groups_lists.forEach(obj => {
            obj.selected = true;
            this.groups_lists.forEach(objx => {
                if(objx.id == obj.id)
                {
                  objx.selected = true;
                }
            })
          })
      });
    }
  }
  groups_listselected(item:Groups)
  {
    
    return item.selected == true
  }

  back()
  {
     this.router.navigate(['/users' ]);
  }
  DELETE()
  {
   
    this.groups.Delete(this.model.id).subscribe((data:any) => {
      this.router.navigate(['/users' ]);

    });
    
  }
  SELECT_COUNTRY(item:Groups)
  {
    if(item.selected == false )
    {
      item.selected = true; 
      this.model.groups_lists.push(item)
    }else{
      item.selected = false; 
      this.model.groups_lists = this.model.groups_lists.filter(objx => objx.id !== item.id);
    }
   
  }
}
