import { Component, OnInit } from '@angular/core';
import { Data,LangCode } from '.././translations'
import { TranslationsService } from '../../translations/translations.service'
import { Router,ActivatedRoute } from '@angular/router';
import { CustomMethodsService } from '../../../../libraries/utils/custommethods.service';
import { AdviableVar } from '../../../../libraries/utils/customvars';
import { Globals } from "../../main/globals";
import { BaseEditComponent } from "../../../../libraries/utils/components/baseedit.component";
@Component({
  selector: 'app-translations_edit',
  templateUrl: './translations.component.html',
  styleUrls: ['./translations.component.css']
})
export class TranslationsEditComponent extends BaseEditComponent {
  model:Data;
  lang_codes: LangCode[];
   constructor(  
    private custom_methodsx:CustomMethodsService,
    private routerx: Router,
    private routex:ActivatedRoute, 
    private categoriesx:TranslationsService,
    private globalx:Globals) {
      super(custom_methodsx,routerx,routex,categoriesx,globalx);
      this.list_url = '/translations/';
   }

 
  initialize()
  {

    this.lang_codes = [
      {
        id:"en",
        name:"en",
      },
      {
        id:"es",
        name:"es",
      },
    ]
    const data = {
      key:"",
      lang_code:"",
      content:"",
      id:null,
      publish:true,
      checked:false,
      selected:false
    };
    this.model = data;
  }
  

}
