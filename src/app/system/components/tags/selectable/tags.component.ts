import { Component, OnInit } from '@angular/core';
import { TagsService } from '../../tags/tags.service'
import { Data,Pagination } from '.././tags'
import { Router } from '@angular/router';
import { Globals } from "../../main/globals";
import { AfterViewInit, ElementRef, EventEmitter, Input, Output, ViewChild } from '@angular/core';
import { BaseListItemSelectableComponent } from "../../../../libraries/utils/components/baseselectable.component";

@Component({
  selector: 'app-tags_selectable',
  templateUrl: './tags.component.html',
  styleUrls: ['./tags.component.css']
})
export class TagsSelectableComponent extends BaseListItemSelectableComponent {

  items: Data[];
  @Input() model: Data[];
  @Output() modelChange = new EventEmitter<Data[]>();
  constructor(
      private routerx: Router, 
      private globalx:Globals,
      private categoriesx:TagsService,
      private elementRefx: ElementRef
    ) {

      super(routerx, globalx,categoriesx, elementRefx);
      this.edit_url = '/tags/edit/';
      this.new_url ='/tags/new';
   }

 
}
