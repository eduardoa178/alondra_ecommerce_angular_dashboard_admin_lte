import { Component, OnInit } from '@angular/core';
import { TagsService } from '../../tags/tags.service'
import { Data,Pagination } from '.././tags'
import { Router } from '@angular/router';
import { Globals } from "../../main/globals";
import { BaseListItemsComponent } from "../../../../libraries/utils/components/baselist.component";
@Component({
  selector: 'app-tags',
  templateUrl: './tags.component.html',
  styleUrls: ['./tags.component.css']
})
export class TagsComponent extends BaseListItemsComponent {
  page:number;
  items: Data[];
  query:string;
  selected:boolean;
  pages: number;
   constructor(
      private  routerx: Router, 
      private globalx:Globals,
      private categoriesx:TagsService
    ) { 
      super(routerx,globalx,categoriesx);
      this.new_url='/tags/new/';
      this.edit_url ='/tags/edit/';
   }
 
}
