import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, } from '@angular/common/http';
import { Headers } from '@angular/http';
import { RequestOptions, Request, RequestMethod} from '@angular/http';
import { environment } from '../../../../environments/environment';
import { Globals } from "../main/globals";
import { CookieService } from 'ngx-cookie-service';
import { HttpJsonHeaderOptionsService } from '../../../libraries/utils/httpoptions.service';
import { BaseService } from "../../../libraries/utils/components/base.service";

@Injectable({
  providedIn: 'root'
})
export class TagsService extends BaseService{

  
  constructor(
    private httpx: HttpClient, 
    private cookiex:CookieService,
    private globalx:Globals, 
    private headerx: HttpJsonHeaderOptionsService) { 
      super(httpx,cookiex, globalx, headerx)
      this.list_url="tags/";
      this.search_url="tags/search/";
      this.new_url="tag/";
      this.get_url="tag/details/";
      this.find_url="tag/find/";
      //this.find_slug_url="";
      this.update_url="tag/";
      this.delete_url="tag/";
  }

  

}
