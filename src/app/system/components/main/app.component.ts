import { Component } from '@angular/core';
import {TranslateService} from '@ngx-translate/core';
import { Location } from '@angular/common';
import { Router, NavigationStart, NavigationEnd, Event as NavigationEvent } from '@angular/router';

/**
CREAR A FUTURO UN DASHBOARD QUE NO DEPENDA DEL USO DIRECTO DE LOS ELEMENTOS QUE TIENE PORQUE ME TIENE JODIDO

**/

declare var jQuery:any;
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent 
{
  title = 'app';
  route: string;
  show: boolean = false;
  router:Router;
  
  constructor(translate: TranslateService, private _router: Router,location: Location) 
  {
      
        // this language will be used as a fallback when a translation isn't found in the current language
        translate.setDefaultLang('es');
        this.router = _router
         // the lang to use, if the lang isn't available, it will use the current loader to get them
        translate.use('es');
         this.router.events.subscribe((val) => 
         {
           var routers = ["/login","/newaccount"];
            this.route = location.path();
            
            if (routers.indexOf(this.route)  != -1)
            {
              this.show = false;
              this.showLogin();
              
            }else
            {
              this.show = true;
              this.removeLogin();
            }
         

        });
        


    }
    removeLogin(){
      jQuery("body").removeClass("login-page");
      jQuery("body").addClass("sidebar-mini");
      jQuery("body").addClass("skin-blue");
    }
    showLogin(){
      jQuery("body").removeClass("sidebar-mini");
      jQuery("body").removeClass("skin-blue");
      jQuery("body").addClass("login-page");
     
    }
    ngOnInit() {
      this.router.navigate(['/login']);
    }
}
